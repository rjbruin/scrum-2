class CreateBoardCategories < ActiveRecord::Migration
  def change
    create_table :board_categories do |t|
      t.references :board, index: true
      t.references :category, index: true

      t.timestamps
    end
  end
end
