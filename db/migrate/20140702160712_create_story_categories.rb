class CreateStoryCategories < ActiveRecord::Migration
  def change
    create_table :story_categories do |t|
      t.references :story, index: true
      t.references :category, index: true

      t.timestamps
    end
  end
end
