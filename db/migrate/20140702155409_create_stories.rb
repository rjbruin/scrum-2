class CreateStories < ActiveRecord::Migration
  def change
    create_table :stories do |t|
      t.text :text
      t.decimal :points
      t.references :user, index: true

      t.timestamps
    end
  end
end
