class BoardCategory < ActiveRecord::Base
  belongs_to :board
  belongs_to :category
end
