json.array!(@boards) do |board|
  json.extract! board, :id, :name, :description, :public
  json.url board_url(board, format: :json)
end
